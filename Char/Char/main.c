#include <stdio.h>
#include <stdlib.h>

char Input[1024];

int SizeOfChar()
{
	int i = 0;
	gets(Input);
	if (Input == NULL) return 0;
	while (Input[i]) i++;
	return i;
}

void CharReverse(int Size)
{
	for (int i = Size; i >= 0; i--)
		printf("%c", Input[i]);
}

int main()
{
	int Size = SizeOfChar();
	printf("Size = %d", Size);
	CharReverse(Size);
	return 0;
}
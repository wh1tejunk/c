#include "element.h"
#include <math.h>

double sinE(double x, int n)
{
	double el = 0;
	int factorial = 1;
	if (n != 0)
		for (int i = 1; i <= (2*n + 1); i++)
			factorial *= i;
	if (n % 2 == 0)
		el = pow(x, (2 * n + 1)) / factorial;
	else el = ((-1) * pow(x, (2 * n + 1))) / factorial;
	return el;
}

double eE(double x, int n)
{
	double el = 0;
	int factorial = 1;
	if (n != 0)
		for (int i = 0; i <= n; i++)
			factorial *= i;
	el = pow(x, n) / factorial;
	return el;
}

double logE(double x, int n)
{
	double el = 0;
	if (n % 2 == 0)
		el = pow(x, n + 1) / (n + 1);
	else
		el = ((-1) * pow(x, n + 1)) / (n + 1);
	return el;
}
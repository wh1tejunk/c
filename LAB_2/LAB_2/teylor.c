#include "teylor.h"

double teylor(element f, double x, int length)
{
	double sum = 0;
	for (int i = 0; i < length; i++)
		sum += f(x, i);
	return sum;
}
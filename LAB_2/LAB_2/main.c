#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <locale.h>
#include "element.h"
#include "teylor.h"

int main()
{
	setlocale(LC_ALL, "Russian");
	double x, result = 0, m_result = 0, definition = 0;
	int selector = 1, size;
	element f = NULL;
	while (selector)
	{
		printf("***************\n\n");
		printf("�������:\n1 - ����������\n2 - �����\n3 - ln(1 + x)\n0 - ����� �� ���������\n������� ����� �������: ");
		scanf_s("%d", &selector);
		if (selector == 0)
			break;
		printf("������� ���������� �����: ");
		scanf("%lf", &x);
		printf("������� ���������� �����: ");
		scanf("%d", &size);
		switch (selector)
		{
		case 1: f = eE;
			m_result = exp(x);
			break;
		case 2: f = sinE;
			m_result = sin(x);
			break;
		case 3: f = logE;
			m_result = log(1 + x);
			break;
		default:
			break;
		}
		result = teylor(f, x, size);
		definition = m_result - result;
		printf("��������� ������� � ����� %f: %lf.\n������: %f\n", x, result, definition);
		printf("***************\n\n");
		selector = 0;
	}
}
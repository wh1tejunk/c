#include "teylor.h"

long double teylor(element f, double x, int length)
{
	long double sum = 0;
	for (int i = 0; i < length; i++)
		sum += f(x, i);
	return sum;
}
#include "element.h"
#include <math.h>

long double sinE(double x, int n)
{
	long double el = 0;
	int factorial = 1;
	int COUNT = 2 * n + 1;
	if (n != 0)
		for (int i = 1; i <= COUNT; i++)
			factorial *= i;
	if (x != 0)
	{
		if (n % 2 == 0)
			el = pow(x, COUNT) / factorial;
		else
			el = (-1) * pow(x, COUNT) / factorial;
	}
	else
		el = 0;
	if ((el == INFINITY) || (el == (-1) * INFINITY)) el = 0;
	return el;
}

long double eE(double x, int n)
{
	long double el = 0;
	int factorial = 1;
	if (n != 0)
		for (int i = 1; i <= n; i++)
			factorial *= i;
	el = pow(x, n) / factorial;
	if ((el == INFINITY) || (el == (-1) * INFINITY)) el = 0;
	return el;
}

long double logE(double x, int n)
{
	long double el = 0;
	int COUNT = n + 1;
	if (n % 2 == 0)
		el = pow(x, COUNT) / (n + 1);
	else
		el = (-1) * pow(x, COUNT) / COUNT;
	if ((el == INFINITY) || (el == (-1) * INFINITY)) el = 0;
	return el;
}
#include <stdio.h>
#include "lab_1.h"
#include <malloc.h>
#include <stdlib.h>
#include <time.h>
#include <locale.h>

int main()
{
	setlocale(LC_ALL, "Russian");
	int *a;
	int *b;
	int *c;
	long avgSortSelect = 0, avgSortBubble = 0, avgSeacrhLin = 0, avgSearchBin = 0;
	int size, search, countOfExp, temp;
	printf("���������� ���������: ");
	scanf_s("%d", &size);
	do
	{
		a = (int*)malloc(size * sizeof(int));
		b = (int*)malloc(size * sizeof(int));
		c = (int*)malloc(size * sizeof(int));
		if (size < 0) printf("���������� ��������� ������ ���� 0\n");
	} while ((size < 0) && (a != NULL) && (b != NULL) && (c != NULL));
	do
	{
		printf("������� ���������� �������������: ");
		scanf_s("%d", &countOfExp);
		if (countOfExp == 0) printf("������� ������������ ��������!");
	} while (countOfExp <= 0);
	int count = countOfExp;
	printf("�������� �������� ��������: ");
	scanf_s("%d", &search);
	srand(time(NULL));
	while (countOfExp != 0)
	{

		for (int i = 0; i < size; i++)
		{
			a[i] = rand() % 1000;
			b[i] = a[i];
			c[i] = a[i];
		}
		bubblesort(a, size);
		avgSortBubble += ret();
		selectsort(b, size);
		avgSortSelect += ret();
		temp = linearSearch(c, search, size);
		avgSeacrhLin += ret();
		if (temp != -1)
			printf("��������������� %d �������!\n", temp);
		temp = binarySearch(a, search, size);
		avgSearchBin += ret();
		if (temp == -1) printf("������ �������� ��� � �������!\n");
		countOfExp--;
	}
	avgSeacrhLin /= count;
	avgSearchBin /= count;
	avgSortBubble /= count;
	avgSortSelect /= count;
	printf("\n ��������:\n ��� ��������� ������ - %d;\n ��� ��������� ������ - %d;\n ��� ���������� ��������� - %d;\n ��� ���������� ���������� - %d;\n", avgSeacrhLin, avgSearchBin, avgSortBubble, avgSortSelect);
	free(a);
	free(b);
	free(c);
	scanf_s("%d", &size);
	return 0;
}
#pragma once

void bubblesort(int *x, int size);

void selectsort(int *x, int size);

int linearSearch(int *x, int element, int size);

int binarySearch(int *x, int element, int size);

int ret();
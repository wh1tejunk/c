#include "lab_1.h"

long NOP;

void bubblesort(int *x, int size)
{
	NOP = 0;
	int k = 1;
	while (k)
	{
		k = 0;
		for (int i = 0; i < size - 1; i++)
		{
			NOP++;
			if (x[i] > x[i + 1])
			{
				int s = x[i + 1];
				x[i + 1] = x[i];
				x[i] = s;
				k = 1;
			}
		}
	}
}

void selectsort(int *x, int size)
{
	NOP = 0;
	int minPosition, temp;
	for (int i = 0; i < size; i++)
	{
		minPosition = i;
		for (int j = i + 1; j < size; j++)
		{
			NOP++;
			if (x[minPosition] > x[j])
				minPosition = j;
		}
		temp = x[minPosition];
		x[minPosition] = x[i];
		x[i] = temp;
	}
}

int linearSearch(int *x, int element, int size)
{
	NOP = 0;
	for (int i = 0; i < size; i++)
	{
		NOP++;
		if (x[i] == element) return i;
	}
	return -1;
}

int binarySearch(int *x, int element, int size)
{
	int low, high, middle;
	low = 0;
	high = size - 1;
	NOP = 0;
	while (low <= high)
	{
		NOP++;
		middle = (low + high) / 2;
		if (element < x[middle])
			high = middle - 1;
		else if(element > x[middle])
			low = middle + 1;
		else
			return middle;
	}
	return -1;
}

int ret()
{
	return NOP;
}